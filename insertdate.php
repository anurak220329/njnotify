<html>
<?php header('Content-type:text/html; charset=utf-8'); ?> 
<?php
       
        include './class/tech.php';
        $tech_obj = new tech();
        
        ?>  
        <head>
  
  </style>
    <title>nj system</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<!-- CSS Files -->
<link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="./assets/css/paper-kit.css?v=2.2.0" rel="stylesheet" />
<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="./assets/demo/demo.css" rel="stylesheet" /><link rel="stylesheet" href="public/theme.css">
</head>
<?php

if (isset($_POST['create'])) {
    $tech_obj->add($_POST);
}
?>
<div class="container"> 
    <div class="row content">
       
        <h3>เพิ่มข้อมูล</h3>
        <hr/>
       
        <div class="container">
        <form method="post" action="">
            <div class="form-group">
            </br>
                <label for="name">ชื่อ-นามสกุล:</label>
                <input type="text" name="name" id="name" class="form-control" required maxlength="50">
            </div>
            <div class="form-group">
            </br>
            <label for="name">เลขร.ว.12:</label>
                <input type="text" name="no" id="no" class="form-control" required maxlength="50">
            </div>
            <div class="form-group">
            </br>
            <label for="name">วันที่มานัดรังวัด:</label>
                <input type="date" name="date" id="date" class="form-control" required maxlength="50">
            </div>
            <div class="form-group">
            </br>
            <label for="name">วันที่ออกรังวัด:</label>
                <input type="date" name="datestart" id="datestart" class="form-control" required maxlength="50">
            </div>
            
            <div class="form-group">
            </br>
            <label for="name">วันครบกำหนดจ่ายเงิน:</label>
                <input type="date" name="datestop" id="datestop" class="form-control" required maxlength="50">
            </div>
            
            
         
            <input type="submit" class="btn btn-outline-info btn-round" name="create" value="เพิ่มวันรังวัด"/>
        </form> 
    </div>
</div>
</div>
<hr/>

</html>
